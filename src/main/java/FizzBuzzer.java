public class FizzBuzzer {

  int divisibleBuzz = 5;
  int divisibleFizz = 3;

  public String calculate(int number) {
    validateNumber(number);
    if (isMultiple(number, divisibleBuzz) && isMultiple(number, divisibleFizz)) return "fizzBuzz";
    if (isMultiple(number, divisibleBuzz)) return "buzz";
    if (isMultiple(number, divisibleFizz)) return "fizz";

    return Integer.toString(number);
  }

  private void validateNumber(int number) {
    if(number < 1 || number > 100) throw new NotFizzBuzzerNumberException();
  }

  private boolean isMultiple(int number, int divisible) {
    return number % divisible == 0;
  }
}
