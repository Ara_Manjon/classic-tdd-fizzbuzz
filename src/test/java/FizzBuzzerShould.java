import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FizzBuzzerShould {
  @Test
  public void for_number_1_get_back_a_string_represented_it() {
    int number = 1;
    String numberToString = "1";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void for_number_2_get_back_a_string_represented_it() {
    int number = 2;
    String numberToString = "2";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void for_number_4_get_back_a_string_represented_it() {
    int number = 4;
    String numberToString = "4";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void for_number_3_get_back_fizz() {
    int number = 3;
    String numberToString = "fizz";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void for_number_6_get_back_fizz() {
    int number = 6;
    String numberToString = "fizz";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void for_number_9_get_back_fizz() {
    int number = 33;
    String numberToString = "fizz";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void for_number_5_get_back_buzz() {
    int number = 5;
    String numberToString = "buzz";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void for_number_25_get_back_buzz() {
    int number = 5;
    String numberToString = "buzz";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void for_number_20_get_back_buzz() {
    int number = 20;
    String numberToString = "buzz";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void for_number_15_get_back_fizzBuzz() {
    int number = 15;
    String numberToString = "fizzBuzz";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void for_number_30_get_back_fizzBuzz() {
    int number = 30;
    String numberToString = "fizzBuzz";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void for_number_45_get_back_fizzBuzz() {
    int number = 45;
    String numberToString = "fizzBuzz";
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    String fizzBuzzerResult = fizzBuzzer.calculate(number);

    Assertions.assertEquals(numberToString, fizzBuzzerResult);
  }

  @Test
  public void not_allow_a_negative_number_as_a_number_to_calculate() {
    int number = -1;
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    Assertions.assertThrows(NotFizzBuzzerNumberException.class, () -> fizzBuzzer.calculate(number));
  }

  @Test
  public void not_allow_a_number_equals_0_as_a_number_to_calculate() {
    int number = 0;
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    Assertions.assertThrows(NotFizzBuzzerNumberException.class, () -> fizzBuzzer.calculate(number));
  }

  @Test
  public void not_allow_a_number_bigger_than_100_as_a_number_to_calculate() {
    int number = 101;
    FizzBuzzer fizzBuzzer = new FizzBuzzer();

    Assertions.assertThrows(NotFizzBuzzerNumberException.class, () -> fizzBuzzer.calculate(number));
  }
}
