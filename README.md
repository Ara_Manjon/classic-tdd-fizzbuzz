# CLASSIC TDD FizzBuzz
​
This is a kata to practice classic TDD and is available in Cyber-Dojo. See [his blog post](http://codingdojo.org/kata/FizzBuzz/)
​
### Kata: FizzBuzz rules
​
Write a function that takes numbers from 1 to 100 and outputs them as a string, but for multiples of three it returns “Fizz” instead of the number, and for multiples of five it returns “Buzz.” For numbers that are multiples of both three and five, it returns “FizzBuzz.”
​
### Tips: Introduce the concepts of TDD and BabySteps
#### The three laws of TDD
​
    -  You are not allowed to write any more of a unit test that is sufficient to fail, and compilation failures are failures
    -  You are not allowed to write any production code unless it is to make a failing unit test pass
    -  You are not allowed to write any more production code that is sufficient to pass the one failing unit test
​
#### Refactoring and the Rule of Three – baby steps
​
    - The Rule of Three defers duplication minimization until there is enough evidence. Code that does not contain duplication is often referred to as abiding to the DRY (Do not Repeat Yourself) principle.
​
#### Three methods of moving forward in TDD
​
    -  Fake it
    -  Obvious implementation
    -  Triangulation
​
#### Detect axes
​
    - The idea is that we stay in one behavior until we are sure that behavior is completed.
​
#### Naming tests
​
    - Tests should have names that describe a business feature or behavior